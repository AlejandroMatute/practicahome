
    <!--Buscador-->

    <div class="buscador col-9 col-sm-9 col-md-6 col-lg-6 col-xl-5" >

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Buscar...</span>
            </div>
            <input type="search" class="form-control">
        </div>
    </div>	

    </div>	

    <!--Articulos-->

    <div class="offset-1 articulos col-9 col-sm-9 col-md-9 col-lg-9 col-xl-10">					
        <h1><center>Procesadores</center></h1>
    </div>

    <div class="articulos offset articulos col-9 col-sm-9 col-md-9  col-lg-9 col-xl-6" >				
    <div class="carta">

        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card1">
                        <img class="card-img-top" src="img/i3.jpg" alt="Card image cap">
                        <h2><center>Intel i3</center></h2>
                </div>
                <div class="atras">
                    <p>Procesador de gama baja</p>
                </div>
            </div>
        </div>

        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card2">
                        <img class="card-img-top" src="img/i5.jpg" alt="Card image cap">
                        <h2><center>Intel i5</center></h2>
                </div>
                <div class="atras">
                    <p>Procesador de gama media</p>
                </div>
            </div>
        </div>

        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card3">
                        <img class="card-img-top" src="img/i7.jpg" alt="Card image cap">
                        <h2><center>Intel i7</center></h2>
                </div>
                <div class="atras">
                    <p>Procesador de gama alta</p>
                </div>
            </div>
        </div>	

    </div>					
    </div>	

