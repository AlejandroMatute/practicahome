// $("#register").submit(function(event) {

$("#register").on('submit', function(e) {

    $.mockjax({
        url : '/foo/bar.html',
        isTimeout: false,
        responseText : 'Server Response Emulated'
        
      });

    validar();    // Funcion donde ponemos las reglas y mensajes personalizados 
    
    if ($("#register").valid()) {    //valid nos dara un Boolean 
        e.preventDefault();
        
        var formData = new FormData($("#register")[0]);  
        var data = CKEDITOR.instances.textarea.getData();
        formData.append("TextArea", data);   

        for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
        }; 


          $.ajax({
            type: 'post',
            url: "/foo/bar.html",
            dataType: 'text',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (responseText) {
                

                $('.title-text').html("Bien hecho!" + " " +responseText);
            },
    
            error: function(data){
                alert("Error :v"); 
            }
        });
        
    }
});

function validar() {

    $("#register").validate({

        ignore: [],
        debug: false,

        rules: {
            name: {
                required: true,
                minlength: 3
            },

            email: {
                required: true,
                email: true
            },

            phone: {
                required: true,
                number: true,
                maxlength: 9,
                minlength: 8,
            },

            textarea:{
                required: function() 
                {
                 CKEDITOR.instances.textarea.updateElement();
                },

                 minlength:10
            },

            state: {
                required: true,
                // minlength: 90000
            }
        },

        messages: {
            "name": {
                required: "Porfavor ingrese su nombre.",
                minlength: "Debe contener 3 caracteres minimo." 
            },

            "email": {
                required: "Porfavor agrega tu mail.", 
                email: "Ingrese un email valido."
            },

            "phone": {
                required: "Introduce tu Telefono.",
                number: "Introduce un numero valido.",
                maxlength: "Debe contener 9 dígitos.",
                minlength: "Debe contener 9 dígitos."
            },

            "textarea":{
                required:"Rellene la area de texto plox",
                minlength:"Minimo 10 caracteres"
            },

            "state": {
                required: "Hola?",
                // minlength:"Hola? >:v"
            }
        }
    }); 
};





$("#auto").click(function(){
    $("#name").val("Alejandro"); 
    $("#email").val("Alejandro@gmail.com");
    $("#phone").val("655678002");
    CKEDITOR.instances['textarea'].setData("¿Qué es Lorem Ipsum? Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. ")
    $("#state").val('3').trigger('change');
});

$("#clear").click(function(){
    $("#name").val(""); 
    $("#email").val("");
    $("#phone").val("");
    CKEDITOR.instances['textarea'].setData("")
});
  
$(":input").each(function() {

    var $this = $(this);    
    
    if ($this.is("textarea")) {
        CKEDITOR.replace( 'textarea' );

    }
});

$(document).ready(function() {
    $('.js-example-basic-single').select2();


}); 